import './App.css';
import Header from './components/header/Header'
import Featured from './components/featured/Featured'
import Games from './components/games/Games'

function App() {
  return (
    <div className="App">
      <div className='site-search'></div>
      <Header />
      <Featured />
      <Games />
    </div>
  );
}

export default App;
