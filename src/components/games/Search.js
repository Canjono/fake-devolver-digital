import { useState } from 'react'
import './Search.css'

export default function Search({amountOfGames, platforms}) {
  const [showPlatforms, setShowPlatform] = useState(false)
  const [showSorts, setShowSorts] = useState(false)

  function renderPlatforms() {
    return platforms.map((platform, index) => {
      return (
        <li key={index}><input type='checkbox' id={index} />
          <span></span>
          <label htmlFor={index}>{platform.name}</label>
        </li>
      )
    })
  }

  return (
    <section className='search'>
      <div className='search__section search__input'>
        <h3>{`${amountOfGames} games`}</h3>
        <div className='input__bar'>
          <input placeholder='Search Games' />
          <span className='material-icons input__icon'>search</span>
        </div>
      </div>

      <div className={`search__section search__platform ${showPlatforms ? 'is-open' : 'is-closed'}`}>
        <div className='toggle-container' onClick={() => setShowPlatform(!showPlatforms)}>
          <div className='search__titles'>
            <h4>platforms</h4>
            <span className='platform__selected'>All</span>
          </div>
          <button className='search__toggle'>
            <span className='material-icons'>expand_more</span>
          </button>
        </div>
        <ul className='platform__list'>
          {renderPlatforms()}
        </ul>
      </div>

      <div className={`search__section search__sort ${showSorts ? 'is-open' : 'is-closed'}`}>
        <div className='toggle-container' onClick={() => setShowSorts(!showSorts)}>
          <div className='search__titles'>
            <h4>sort</h4>
            <span className='sorting-selected'>Newest</span>
          </div>
          <button className='search__toggle'><span className='material-icons'>expand_more</span></button>
        </div>
        <ul className='sort__list'>
          <li key='newest-item'>
            <input type='checkbox' id='newest-input' />
            <span></span>
            <label htmlFor='newest-input'>Newest</label>
          </li>
          <li key='oldest-item'>
            <input type='checkbox' id='oldest-input' />
            <span></span>
            <label htmlFor='oldest-input'>Oldest</label>
          </li>
          <li key='name-item'>
            <input type='checkbox' id='name-input' />
            <span></span>
            <label htmlFor='name-input'>Name (A-Z)</label>
          </li>
          <li key='eman-item'>
            <input type='checkbox' id='eman-input' />
            <span></span>
            <label htmlFor='eman-input'>(Z-A) emaN</label>
          </li>
          <li key='i-dont-care-item'>
            <input type='checkbox' id='i-dont-care-input' />
            <span></span>
            <label htmlFor='i-dont-care-input'>I dont care</label>
          </li>
        </ul>
      </div>
    </section>
  )
}
