export default function GameList({games}) {
  function renderGameCards() {
    return games.map(game => {
      return (
        <div key={game.id} className='game-card'>
          <h1>{game.name}</h1>
        </div>
      )
    })
  }

  return (
    <section className='game-list'>
      {/* {renderGameCards()} */}
    </section>
  )
}