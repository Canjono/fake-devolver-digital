import Search from './Search'
import Tags from './Tags'
import GameList from './GameList'
import './Games.css'
import GameService from '../../services/GameService'
import PlatformService from '../../services/PlatformService'

export default function Games() {
  const games = GameService.getAll()
  const platforms = PlatformService.getAll()

  return (
    <section className='games'>
      <Tags />
      <Search amountOfGames={games.length} platforms={platforms}/>
      {/* <GameList games={games} /> */}
    </section>
  )
}