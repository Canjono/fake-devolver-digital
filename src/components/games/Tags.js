import React, { useState } from 'react'
import TagService from '../../services/TagService'
import './Tags.css'

export default function Tags() {
  const [tagListOpen, setTagListOpen] = useState(false)
  const [selectedTags, setSelectedTags] = useState([])

  function toggleTagList() {
    setTagListOpen(!tagListOpen)
  }

  function getAllTags() {
    return TagService.getAll().map((tag, index) => {
      return <button key={index} className={`tag ${tag.id}`} onClick={() => selectTag(tag.id)}>{ tag.text }</button>
    })
  }

  function selectTag(id) {
    const isSelected = selectedTags.find(tag => tag === id)
    if (isSelected) {
      setSelectedTags(selectedTags.filter(tag => tag !== id))
    } else {
      selectedTags.push(id)
      setSelectedTags(selectedTags)
    }
  }

  return (
    <section className={`tags ${tagListOpen ? 'tag-grid__open' : 'tag-grid__closed'}`}>
      <div className='tag-grid-container'>
        <h1>Discover our games</h1>
        <div className='tag-grid'>
          {getAllTags()}
        </div>
      </div>
      <div className='tags-toggle' onClick={toggleTagList}>
        <span className='tags-toggle__text'>All tags</span>
        <span className='material-icons tags-toggle__arrow'>arrow_forward_ios</span>
        <span className='material-icons tags-toggle__close'>close</span>
      </div>
    </section>
  )
}
