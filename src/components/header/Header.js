import React, { useState } from 'react'
import './Header.css'

export default function Header() {
  const [hamburgerIsOpen, setHamburgerIsOpen] = useState(false)

  function toggleHamburger() {
    setHamburgerIsOpen(!hamburgerIsOpen)
  }

  return (
    <header className={`header ${hamburgerIsOpen ? 'is-open' : 'is-closed'}`}>
      <button className='hamburger' href='#' onClick={toggleHamburger}>
        <div className='hamburger-icon hamburger__open'>
          <span className='material-icons'>menu</span>
        </div>
        <div className='hamburger-icon hamburger__close'>
          <span className='material-icons'>close</span>
        </div>
      </button>
      <div className='logo-container'>
        <a className='logo' href='/'></a>
      </div>
    </header>
  )
}
