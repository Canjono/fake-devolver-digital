import React from 'react'
import './Featured.css'

function Featured() {
  return (
    <section className='featured'>
      <div className='image'></div>
      <div className='details'>
        <div className='title'><span>cult of the lamb</span></div>
        <div className='subtitle'><span>Ascending to pc and console in 2022</span></div>
        <div className='platform'></div>
        <div className='link'><a href='#'>Check it out<span className="material-icons">east</span></a></div>
      </div>
    </section>
  )
}

export default Featured
