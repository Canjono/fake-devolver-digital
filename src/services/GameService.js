export default class GameService {
  static getAll() {
    return [
      { id: 'cult-of-the-lamb', title: 'Cult of the Lamb', videoLink: '' },
      { id: 'demon-throttle', title: 'Demon Throttle', videoLink: '' },
      { id: 'terra-nil', title: 'Terra Nil', videoLink: '' },
      { id: 'wizard-with-a-gun', title: 'Wizard with a Gun', videoLink: '' },
      { id: 'trek-to-yomi', title: 'Trek to Yomi', videoLink: '' },
      { id: 'shadow-warrior-3', title: 'Shadow Warrior 3', videoLink: '' },
      { id: 'inscryption', title: 'Inscryption', videoLink: '' },
      { id: 'devolver-tumble-time', title: 'Devlver Tumble Time', videoLink: '' },
    ]
  }
}