export default class PlatformService {
  static getAll() {
    return [
      { id: 'playstation-4', name: 'Playstation 4' },
      { id: 'playstation-3', name: 'Playerstation 3' },
      { id: 'xbox-one', name: 'Xbox One' },
      { id: 'windows-pc', name: 'Windows PC' },
      { id: 'apple-mac', name: 'Apple Mac' },
      { id: 'android', name: 'Android' },
      { id: 'ios', name: 'iOS' },
      { id: 'nintendo-switch', name: 'Nintendo Switch' },
      { id: 'linux', name: 'Linux' },
      { id: 'htc-vive', name: 'HTC Vive' },
      { id: 'oculus', name: 'Oculus' },
      { id: 'snes', name: 'SNES' },
      { id: 'consoles', name: 'Consoles' },
      { id: 'apple-arcade', name: 'Apple Arcade' },
      { id: 'psvr', name: 'PSVR' },
      { id: 'stadia', name: 'Stadia' }
    ]
  }
}